
<!-- README.md is generated from README.Rmd. Please edit that file -->

## Launch application

open R and paste the following code :

``` r
install.packages("remotes")
remotes::install_gitlab(host = "https://gitlab.univ-lr.fr", repo = "pelaverse/pbrfrtuning")
library(pbrFrTuning)
run_app()
```
