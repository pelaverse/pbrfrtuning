---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```



## Launch application

open R and paste the following code :

```{r lauch, eval = FALSE}
install.packages("remotes")
remotes::install_gitlab(host = "https://gitlab.univ-lr.fr", repo = "pelaverse/pbrfrtuning")
library(pbrFrTuning)
run_app()
```

